import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class Email{
	public static void main(String[] args){
		// Setting up paramters
		String sendTo = "a0126172m@cs2107.win";
		String sendFrom = "cs2107ta@cs2107.win";
		
		
		String host = "smtp.live.com";
		
		// Get system properties
		Properties properties = new Properties();

		// Setup mail server
		properties.setProperty("mail.from", "cs2107ta@cs2107.win");
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", "25");
		
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.debug", "true");
		properties.put("mail.smtp.auth", "true");

		// Get the default Session object.
		// Get the Session object.
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);
			
			// Set From: header field of the header.
			message.setFrom(new InternetAddress(sendFrom));
			
			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendTo));
			
			// Set Subject: header field
			message.setSubject("Testing Subject");
			
			// Now set the actual message
			message.setText("Hello, this is sample for to check send " + "email using JavaMailAPI ");

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		
	}
}
