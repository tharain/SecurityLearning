import smtplib

fromshow = 'cs2107ta@cs2107.win'
toaddy = ['a0126172m@cs2107.win']
subject = ' test'
body = 'This is the body test'

content = '''\
From: %s
To: %s
Subject: %s
%s
''' % (fromshow, ', '.join(toaddy), subject, body)

server = 'smtp.live.com'
port = 80

mail = smtplib.SMTP(server, port)

mail.ehlo()
mail.starttls()
try:
    mail.sendmail(fromshow, toaddy, content)
    print('E-mail sent.')
except:
    print('E-mail not sent.')

mail.close()
